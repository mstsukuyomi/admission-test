package com.manimal.mlshop.di.module

import com.manimal.mlshop.BuildConfig
import com.manimal.mlshop.common.services.MercadoLibreService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
class RetroModule {

    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Provides
    fun provideMercadoLibreService(retrofit: Retrofit): MercadoLibreService {
        return retrofit.create(MercadoLibreService::class.java)
    }
}