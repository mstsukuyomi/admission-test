package com.manimal.mlshop.di.module

import androidx.lifecycle.ViewModel
import com.manimal.mlshop.di.annotation.ViewModelKey
import com.manimal.mlshop.screens.main.viewmodel.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindSplashViewModel(viewModel: MainViewModel): ViewModel
}