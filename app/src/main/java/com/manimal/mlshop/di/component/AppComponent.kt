package com.manimal.mlshop.di.component

import android.app.Application
import com.manimal.mlshop.App
import com.manimal.mlshop.di.module.ActivityModule
import com.manimal.mlshop.di.module.AppModule
import com.manimal.mlshop.di.module.RetroModule
import com.manimal.mlshop.di.module.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityModule::class,
        ViewModelModule::class,
        AppModule::class,
        RetroModule::class
    ]
)
interface AppComponent : AndroidInjector<App> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    override fun inject(instance: App?)
}