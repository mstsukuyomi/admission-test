package com.manimal.mlshop.screens.main.view.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import com.bumptech.glide.Glide
import com.manimal.mlshop.R
import com.manimal.mlshop.databinding.FragmentDetailProductBinding
import com.manimal.mlshop.screens.main.viewmodel.MainViewModel

class DetailProductFragment : Fragment() {

    private lateinit var binding: FragmentDetailProductBinding
    private val viewModel: MainViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_detail_product, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.viewModel = viewModel

        Glide.with(binding.ivProductImage.context).load(viewModel.selectedItem.value?.thumbnail)
                .into(binding.ivProductImage)

        setupToolbar()
    }

    private fun setupToolbar() {
        binding.toolbar.tvToolTitle.setText(R.string.title_search_result)
        binding.toolbar.ivBack.setOnClickListener {
            requireActivity().onBackPressed()
        }
    }
}