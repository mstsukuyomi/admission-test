package com.manimal.mlshop.screens.main.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.manimal.mlshop.R
import com.manimal.mlshop.databinding.ItemSearchResultBinding
import com.manimal.mlshop.screens.main.model.ResultModel
import com.manimal.mlshop.utils.FormatHelper

class ResultsAdapter(val onItemClick: ((item: ResultModel) -> Unit)? = null) : RecyclerView.Adapter<ResultsAdapter.ResultsViewHolder>() {

    lateinit var binding: ItemSearchResultBinding
    private var listItems :MutableList<ResultModel> = mutableListOf()

    fun refreshData(data: List<ResultModel>) {
        listItems.clear()
        listItems.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ResultsViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_search_result, parent, false)
        return ResultsViewHolder(binding)
    }

    override fun getItemCount(): Int = listItems.size

    override fun onBindViewHolder(holder: ResultsViewHolder, position: Int) {
        holder.bind(listItems[position])
    }

    inner class ResultsViewHolder(val binding: ItemSearchResultBinding): RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ResultModel) {
            binding.tvProductTitle.text = item.title
            binding.tvProductPrice.text = FormatHelper.buildCoinFormat(item.price)

            Glide.with(binding.ivProductImage.context).load(item.thumbnail)
                    .into(binding.ivProductImage)

            binding.root.setOnClickListener {
                onItemClick?.invoke(item)
            }
        }
    }
}