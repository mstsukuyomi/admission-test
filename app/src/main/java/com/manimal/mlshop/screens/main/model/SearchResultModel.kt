package com.manimal.mlshop.screens.main.model

import com.google.gson.annotations.SerializedName

data class SearchResultModel(
    @SerializedName("site_id")
    val siteId: String? = "",
    @SerializedName("query")
    val query: String? = "",
    @SerializedName("results")
    val results: List<ResultModel> = mutableListOf()
)