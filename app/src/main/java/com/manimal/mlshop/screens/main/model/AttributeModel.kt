package com.manimal.mlshop.screens.main.model

import com.google.gson.annotations.SerializedName

data class AttributeModel(
        @SerializedName("name")
        val name: String = "",
        @SerializedName("value_name")
        val valueName: String = "",
        @SerializedName("value_id")
        val valueId: String = ""
)