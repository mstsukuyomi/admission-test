package com.manimal.mlshop.screens.main.repository

import android.util.Log
import com.manimal.mlshop.common.services.MercadoLibreService
import com.manimal.mlshop.screens.main.model.SearchResultModel
import com.manimal.mlshop.utils.Constants
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.UnknownHostException
import javax.inject.Inject

class MainRepository @Inject constructor(private val service: MercadoLibreService) {

    private val tag = MainRepository::class.java.simpleName

    fun searchProducts(
        word: String,
        success: (value: SearchResultModel?)-> Unit,
        error: (value: String?)-> Unit
    ) {
        val search = service.search(word)
        search?.enqueue(object : Callback<SearchResultModel?> {
            override fun onResponse(call: Call<SearchResultModel?>?, response: Response<SearchResultModel?>) {
                if (response.isSuccessful && response.body() != null) {
                    success(response.body())
                } else {
                    Log.d(tag, "ErrorCode: " + response.code())
                    error(Constants.ERROR_GENERIC_MESSAGE)
                }
            }

            override fun onFailure(call: Call<SearchResultModel?>?, t: Throwable?) {
                t?.let {
                    Log.d(tag, t.message, t)
                    if (t is UnknownHostException) {
                        error(Constants.ERROR_INTERNET_MESSAGE)
                    } else {
                        error(Constants.ERROR_GENERIC_MESSAGE)
                    }
                }
            }
        })
    }
}