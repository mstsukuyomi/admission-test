package com.manimal.mlshop.screens.main.view

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.manimal.mlshop.R
import com.manimal.mlshop.common.base.BaseActivity
import com.manimal.mlshop.databinding.ActivityMainBinding
import com.manimal.mlshop.screens.main.viewmodel.MainViewModel

class MainActivity : BaseActivity() {

    lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityMainBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_main)

        viewModel =
            ViewModelProviders.of(this, viewModelFactory)[MainViewModel::class.java]
        binding.viewModel = viewModel
        setBaseViewModel(viewModel)
    }
}