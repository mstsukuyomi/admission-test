package com.manimal.mlshop.screens.main.viewmodel

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.manimal.mlshop.common.base.BaseViewModel
import com.manimal.mlshop.screens.main.model.ResultModel
import com.manimal.mlshop.screens.main.repository.MainRepository
import javax.inject.Inject

class MainViewModel  @Inject constructor(
    private val mainRepository: MainRepository
): BaseViewModel() {

    private val _results = MutableLiveData<List<ResultModel>>()
    val results: LiveData<List<ResultModel>> get() = _results

    val enableSearchButton = ObservableBoolean()

    var tempWord: String = ""

    val selectedItem = MutableLiveData<ResultModel>()

    fun search(word: String) {
        showLoading.set(true)
        mainRepository.searchProducts(
            word = word,
            success =  { successResult ->
                _results.value = successResult?.results
                showLoading.set(false)
            },
            error = { errorMessage ->
                genericError.value = errorMessage
                showLoading.set(false)
            }
        )
    }

    fun handleTextChanges(text: CharSequence?) {
        enableSearchButton.set(!text.isNullOrBlank())
    }
}