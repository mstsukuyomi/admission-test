package com.manimal.mlshop.screens.main.model

import com.google.gson.annotations.SerializedName

data class ResultModel (
    @SerializedName("id")
    val id: String = "",
    @SerializedName("site_id")
    val siteId: String = "",
    @SerializedName("title")
    val title: String = "",
    @SerializedName("price")
    val price: Double = 0.0,
    @SerializedName("available_quantity")
    val availableQuantity: Int = 0,
    @SerializedName("sold_quantity")
    val soldQuantity: Int = 0,
    @SerializedName("condition")
    val condition: String = "",
    @SerializedName("thumbnail")
    val thumbnail: String = "",
    @SerializedName("attributes")
    val attributes: List<AttributeModel> = arrayListOf()
)