package com.manimal.mlshop.screens.main.view.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.manimal.mlshop.R
import com.manimal.mlshop.databinding.FragmentMainBinding
import com.manimal.mlshop.screens.main.viewmodel.MainViewModel

class MainFragment : Fragment() {

    private lateinit var binding: FragmentMainBinding
    private val viewModel: MainViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupListeners()
    }

    override fun onResume() {
        super.onResume()
        binding.include.etSearch.setText("")
    }

    private fun setupListeners() {
        binding.include.viewModel = viewModel

        binding.include.etSearch.doOnTextChanged { text, _, _, _ ->
            viewModel.handleTextChanges(text)
        }

        binding.include.ivSearch.setOnClickListener {
            viewModel.tempWord = binding.include.etSearch.text.toString()
            findNavController().navigate(R.id.go_to_searchResultFragment)
        }
    }
}