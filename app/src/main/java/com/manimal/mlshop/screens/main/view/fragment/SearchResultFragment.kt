package com.manimal.mlshop.screens.main.view.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.manimal.mlshop.R
import com.manimal.mlshop.common.base.BaseActivity
import com.manimal.mlshop.databinding.FragmentSearchResultBinding
import com.manimal.mlshop.screens.main.model.ResultModel
import com.manimal.mlshop.screens.main.view.adapter.ResultsAdapter
import com.manimal.mlshop.screens.main.viewmodel.MainViewModel

class SearchResultFragment : Fragment() {

    private lateinit var binding: FragmentSearchResultBinding
    private val viewModel: MainViewModel by activityViewModels()

    private val resultsAdapter: ResultsAdapter = ResultsAdapter(::onItemClicked)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search_result, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (requireActivity() as BaseActivity).hideKeyboard()

        setupToolbar()
        observe()
        setupListeners()
    }

    private fun setupToolbar() {
        binding.toolbar.tvToolTitle.setText(R.string.title_search_result)
        binding.toolbar.ivBack.setOnClickListener {
            requireActivity().onBackPressed()
        }
    }

    private fun observe() {
        viewModel.results.observe(requireActivity(), Observer {
            if (!it.isNullOrEmpty()) {
                resultsAdapter.refreshData(it)
            }
        })
    }

    private fun setupListeners() {
        binding.include.viewModel = viewModel
        binding.rvResults.adapter = resultsAdapter

        binding.include.etSearch.doOnTextChanged { text, _, _, _ ->
            viewModel.handleTextChanges(text)
        }

        binding.include.ivSearch.setOnClickListener {
            viewModel.search(binding.include.etSearch.text.toString())
        }

        val tempWord = viewModel.tempWord
        if (tempWord.isNotBlank()) {
            binding.include.etSearch.setText(tempWord)
            viewModel.search(tempWord)
        }
    }

    private fun onItemClicked(item: ResultModel) {
        viewModel.selectedItem.value = item
        findNavController().navigate(R.id.go_to_detailProductFragment)
    }
}