package com.manimal.mlshop.common.base

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class BaseViewModel: ViewModel() {

    val showLoading = ObservableBoolean()
    val genericError = MutableLiveData<String>()
}