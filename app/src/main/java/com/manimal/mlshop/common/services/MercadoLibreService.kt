package com.manimal.mlshop.common.services

import com.manimal.mlshop.screens.main.model.SearchResultModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface MercadoLibreService {

    @GET("search")
    fun search(@Query("q") word: String): Call<SearchResultModel?>?
}