package com.manimal.mlshop.common.base

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.manimal.mlshop.R
import com.manimal.mlshop.utils.Constants
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

open class BaseActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private var baseViewModel: BaseViewModel? = null
    lateinit var builder: AlertDialog.Builder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        builder = AlertDialog.Builder(this)
    }

    fun setBaseViewModel(baseViewModel: BaseViewModel) {
        this.baseViewModel = baseViewModel
        this.observeBaseVariables()
    }

    private fun observeBaseVariables() {
        this.baseViewModel?.genericError?.observe(this, Observer { error ->
            processGeneralError(error)
        })
    }

    private fun processGeneralError(error: String) {
        builder.setTitle(R.string.error_generic_title)
        builder.setMessage(error)
        builder.setPositiveButton(R.string.error_generic_ok) { dialog, _ ->
            dialog.dismiss()
        }

        builder.setCancelable(false)
        builder.show()
    }

    internal fun hideKeyboard() {
        val manager = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = this.currentFocus
        if (view == null)
            view = View(this)
        manager.hideSoftInputFromWindow(view.windowToken, 0)
    }
}