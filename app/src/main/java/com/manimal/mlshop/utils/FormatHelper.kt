package com.manimal.mlshop.utils

import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

object FormatHelper {

    fun buildCoinFormat(number: Double?): String {
        return if (number == null) {
            "$0"
        } else {
            val symbol = DecimalFormatSymbols()
            symbol.groupingSeparator = '.'
            symbol.decimalSeparator = ','
            val format = DecimalFormat("$#,###.##", symbol)
            format.format(number)
        }
    }
}