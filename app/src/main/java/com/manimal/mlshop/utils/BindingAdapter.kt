package com.manimal.mlshop.utils

import android.widget.TextView
import androidx.databinding.BindingAdapter

@BindingAdapter("android:currency")
fun setCurrencyText(textView: TextView, double: Double) {
    textView.text = FormatHelper.buildCoinFormat(double)
}