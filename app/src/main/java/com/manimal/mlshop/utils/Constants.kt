package com.manimal.mlshop.utils

class Constants {

    companion object {
        const val ERROR_GENERIC_MESSAGE = "A ocurrido un error inesperado, por favor intente más tarde"
        const val ERROR_INTERNET_MESSAGE = "Por favor verifique su conexión a internet"
    }
}