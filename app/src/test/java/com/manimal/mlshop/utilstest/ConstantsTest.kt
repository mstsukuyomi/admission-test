package com.manimal.mlshop.utilstest

class ConstantsTest {

    companion object {
        const val SEARCH_TEXT_TEST = "Motorola"

        const val TEST_VALUE_TEN_THOUSAND = "$10.000"
        const val TEST_VALUE_ZERO = "$0"
    }
}