package com.manimal.mlshop.utilstest

import org.mockito.Mockito

object TestUtils {

    fun <T> anyObject(): T {
        Mockito.anyObject<T>()
        return uninitialized()
    }

    @Suppress("UNCHECKED_CAST")
    private fun <T> uninitialized(): T = null as T
}