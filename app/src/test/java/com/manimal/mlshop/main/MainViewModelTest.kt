package com.manimal.mlshop.main

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.manimal.mlshop.screens.main.model.SearchResultModel
import com.manimal.mlshop.utilstest.ConstantsTest
import com.manimal.mlshop.screens.main.repository.MainRepository
import com.manimal.mlshop.screens.main.viewmodel.MainViewModel
import com.manimal.mlshop.utils.Constants
import com.manimal.mlshop.utilstest.LiveDataTestUtil
import com.manimal.mlshop.utilstest.TestUtils
import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito

class MainViewModelTest {

    @get:Rule
    var rule = InstantTaskExecutorRule()

    private lateinit var mainRepository: MainRepository
    private lateinit var mainViewModel: MainViewModel

    @Before
    fun setup() {
        mainRepository = Mockito.mock(MainRepository::class.java)
        mainViewModel = MainViewModel(mainRepository)
    }

    @Test
    fun `Search success`() {
        val resultSuccess =
            SearchResultModel()
        Mockito.`when`(
            mainRepository.searchProducts(
                TestUtils.anyObject(),
                TestUtils.anyObject(),
                TestUtils.anyObject()
            )
        ).then { invocation ->
            invocation.getArgument<(value: SearchResultModel)-> Unit>(1).invoke(resultSuccess)
        }

        mainViewModel.search(ConstantsTest.SEARCH_TEXT_TEST)

        assertEquals(
            resultSuccess.results,
            LiveDataTestUtil.getValue(mainViewModel.results)
        )
    }

    @Test
    fun `Search internet problems`() {
        val resultError = Constants.ERROR_INTERNET_MESSAGE
        Mockito.`when`(
            mainRepository.searchProducts(
                TestUtils.anyObject(),
                TestUtils.anyObject(),
                TestUtils.anyObject()
            )
        ).then { invocation ->
            invocation.getArgument<(value: String)-> Unit>(2).invoke(resultError)
        }

        mainViewModel.search(ConstantsTest.SEARCH_TEXT_TEST)

        assertEquals(
            resultError,
            LiveDataTestUtil.getValue(mainViewModel.genericError)
        )
    }

    @Test
    fun `Search service problems`() {
        val resultError = Constants.ERROR_GENERIC_MESSAGE
        Mockito.`when`(
            mainRepository.searchProducts(
                TestUtils.anyObject(),
                TestUtils.anyObject(),
                TestUtils.anyObject()
            )
        ).then { invocation ->
            invocation.getArgument<(value: String)-> Unit>(2).invoke(resultError)
        }

        mainViewModel.search(ConstantsTest.SEARCH_TEXT_TEST)

        assertEquals(
            resultError,
            LiveDataTestUtil.getValue(mainViewModel.genericError)
        )
    }

    @Test
    fun `Validate text listener function wrong inputs`() {
        Assert.assertFalse(mainViewModel.enableSearchButton.get())
        mainViewModel.handleTextChanges("")
        Assert.assertFalse(mainViewModel.enableSearchButton.get())
        mainViewModel.handleTextChanges(" ")
        Assert.assertFalse(mainViewModel.enableSearchButton.get())
        mainViewModel.handleTextChanges(null)
        Assert.assertFalse(mainViewModel.enableSearchButton.get())
        mainViewModel.handleTextChanges(ConstantsTest.SEARCH_TEXT_TEST)
        Assert.assertTrue(mainViewModel.enableSearchButton.get())
    }
}