package com.manimal.mlshop.utils

import com.manimal.mlshop.utilstest.ConstantsTest
import org.junit.Assert
import org.junit.Test

class FormatHelperTest {

    @Test
    fun `Format 10000`() {
        Assert.assertEquals(
            ConstantsTest.TEST_VALUE_TEN_THOUSAND,
            FormatHelper.buildCoinFormat(10000.0)
        )
    }

    @Test
    fun `Format null`() {
        Assert.assertEquals(
            ConstantsTest.TEST_VALUE_ZERO,
            FormatHelper.buildCoinFormat(null)
        )
    }
}