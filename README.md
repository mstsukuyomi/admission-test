## ML Shop

This application lets users to search products with a simple search box, then you can see a found products list with the name, image and price, after that you can tap any item to see it in a detail view.

---

## Minimum SDK supported

API 19, Android 4.4 (KitKat)

---

## Architecture

With a MVVM arquitecture you can find the different components as views or logical classes and with a SOLID implementation it let you understand easily the code and improve, modify or add new functionalities without hurt the existing code. 

1. di contains Dagger components, ViewModelFactory and the different modules to inject the neccesaty dependenies.
2. screens contain the main package, in this case is the unique module in the app and this has different fragments to interact with the distinc steps to search and find the product.
3. screens/main/model with the two necessary data classes to parse the service response.
4. screens/main/repository with the MainRepository who receive the MercadoLibreService and obtein the required data, in this case provided by the MercadoLibre APIs.
5. screens/main/viewmodel with the MainViewModel, it is the in charge of call the searchProducts method in the repository and assign the success or error response to different mutable varibles to be listened for the view, additionaly it has the handleTextChanges who lets enable or disable the searcher button.
6. screens/main/view, it containe the MainActivity who has the main_navigation to provide the application flow through the different fragments.
7. screens/main/view/fragment Contains the MainFragment who has the search editText, SearchResultFragment lists the search service results with the posibility of realize a new search, the DetailProductFragment who shows attributes of the selected product, this selection is realized by a tap on the list on the SearchResultFragment.
8. screens/main/view/adapter with the ResultsAdapter the SearchResultFragment can show the product searching results.

---

## DataBinding

Let a faster response from the MercadoLibre api services to the searching view

---

## Functional and reactive programming

With the functional programming implemented in the MainRepository the MainViewModel can call the searchProducts method and control the specific response without combine this bussines logical with the other methods
The reactive programming lets to send the products information to the different views since the fragment_search_result to fragment_detail_product. additionaly let the faster implementation of the different validation as the EditText products searcher.

---

## Unit test

With mockito implementation we can test the different handle of the methods in the application, it helps to anticipate posible problems with the methos as a NULL or empty values.
